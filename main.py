from models import *
import sys
import argparse


BaseModel().create_tables()



parser = argparse.ArgumentParser()
parser.add_argument("command")
parser.add_argument("--name")
parser.add_argument("--first_name")
parser.add_argument("--last_name")
parser.add_argument("--birthday")
parser.add_argument("--owner_id")
parser.add_argument("--id")
passed_args = parser.parse_args()

if passed_args.command == 'create_owner':
	obj = Owner(first_name=passed_args.first_name, last_name=passed_args.last_name, birthday=passed_args.birthday).save()
	print("Success. Object_id: %s" % obj.id)
elif passed_args.command == 'create_cat':
	obj = Cat(name=passed_args.name, birthday=passed_args.birthday, owner_id=passed_args.owner_id).save()
	print("Success. Object_id: %s" % obj.id)
elif passed_args.command == 'create_dog':
	obj = Dog(name=passed_args.name, birthday=passed_args.birthday, owner_id=passed_args.owner_id).save()
	print("Success. Object_id: %s" % obj.id)


elif passed_args.command == 'update_owner':
	obj = Owner(first_name=passed_args.first_name, last_name=passed_args.last_name, birthday=passed_args.birthday, id=passed_args.id).update()
	print("Success. Object_id: %s" % obj.id)
elif passed_args.command == 'update_cat':
	obj = Cat(name=passed_args.name, birthday=passed_args.birthday, owner_id=passed_args.owner_id, id=passed_args.id).update()
	print("Success. Object_id: %s" % obj.id)
elif passed_args.command == 'update_dog':
	obj = Dog(name=passed_args.name, birthday=passed_args.birthday, owner_id=passed_args.owner_id, id=passed_args.id).update()
	print("Success. Object_id: %s" % obj.id)	


elif passed_args.command == 'delete_owner':
	obj = Owner.delete(passed_args.id)
	print("Success.")
elif passed_args.command == 'delete_cat':
	obj = Cat.delete(passed_args.id)
	print("Success.")
elif passed_args.command == 'delete_dog':
	obj = Dog.delete(passed_args.id)
	print("Success.")		


elif passed_args.command == 'list_owner':
	for obj in Owner.list():
		print(obj)
elif passed_args.command == 'list_cat':
	owner_id = passed_args.owner_id
	for obj in Cat.list():
		if owner_id is not None:
			if obj[1] == owner_id:
				print(obj)
		else:
			print(obj)


elif passed_args.command == 'dog_number':
	print(len([obj for obj in Dog.list() if obj[1] == passed_args.owner_id]))

elif passed_args.command == 'cat_number':
	print(len([obj for obj in Cat.list() if obj[1] == passed_args.owner_id]))









