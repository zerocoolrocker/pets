# Pets Python app


# CREATE COMMANDS

- create_owner  --first_name FIRST_NAME --last_name LAST_NAME --birthday BIRTHDAY
- create_cat --name NAME --first_name FIRST_NAME --last_name LAST_NAME --birthday BIRTHDAY --owner_id OWNER_ID 
- create_dog --name NAME --first_name FIRST_NAME --last_name LAST_NAME --birthday BIRTHDAY --owner_id OWNER_ID 



# UPDATE_COMMANDS

- update_owner --id ID --first_name FIRST_NAME --last_name LAST_NAME --birthday BIRTHDAY
- update_cat --id ID --name NAME --first_name FIRST_NAME --last_name LAST_NAME --birthday BIRTHDAY --owner_id OWNER_ID 
- update_dog --id ID --name NAME --first_name FIRST_NAME --last_name LAST_NAME --birthday BIRTHDAY --owner_id OWNER_ID 


# DELETE_COMMANDS

- delete_owner --id ID
- delete_cat --id ID
- delete_dog --id ID


LIST COMMANDS

- list_owner
- list_dog --owner_id OWNER_ID 
- list_cat --owner_id OWNER_ID 


# COUNT OWNER DOGS AND CATS

- dog_number --owner_id OWNER_ID 
- cat_number --owner_id OWNER_ID 


