import sqlite3
from settings import DB_NAME


conn = sqlite3.connect(DB_NAME)


class BaseModel(object):

	def __connect(self):
		self.conn = sqlite3.connect(DB_NAME)


	def __disconnect(self):	
		self.conn.close()

	def __get_cursor(self):
		return self.conn.cursor()

	def __init__(self, cls=None, **kwargs):
		for key, value in kwargs.items():
			if not '__' in cls .__name__ and (key in dir(cls) or key == 'id'):
				setattr(self, key, value)

	def data(self):
		return {field: hasattr(field, 'id') or getattr(self, field, None) for field in dir(self.__class__) if not '__' in field and not field in ('save', 'update', 'delete', 'list', 'create_tables', 'data')}

	def save(self, cls):
		self.__connect()
		data = self.data()
		cursor = self.__get_cursor()
		ret_data = cursor.execute("INSERT INTO %s(%s) VALUES (%s)" % (cls.__name__.lower(), ','.join(data.keys()), ','.join(['?'] * len(data.values()))), list(data.values()))
		self.conn.commit()
		self.id = ret_data.lastrowid
		self.__disconnect()
		return self


	def update(self, cls):
		self.__connect()
		data = self.data()
		cursor = self.__get_cursor()
		ret_data = cursor.execute("UPDATE %s SET %s WHERE id=%s" % (cls.__name__.lower(), ','.join(["%s=?" % k for k, v in data.items()]), self.id), list(data.values()))
		self.conn.commit()
		self.__disconnect()
		return ret_data


	@staticmethod
	def delete(cls, id):
		conn = sqlite3.connect(DB_NAME)
		c = conn.cursor()
		c.execute("DELETE FROM %s WHERE id=%s" % (cls.__name__.lower(), id))
		conn.commit()
		conn.close()


	@staticmethod
	def list(cls):
		conn = sqlite3.connect(DB_NAME)
		c = conn.cursor()
		ret = c.execute("select * from %s" % cls.__name__.lower()).fetchall()
		conn.close()
		return ret



	def create_tables(self):
		self.__connect()
		self.conn.execute("create table if not exists owner(birthday, first_name, id integer primary key, last_name)")
		self.conn.execute("create table if not exists dog(birthday, owner_id, id integer primary key, name)")
		self.conn.execute("create table if not exists cat(birthday, owner_id, id integer primary key, name)")
		self.__disconnect()



	

class Owner(BaseModel):
	birthday = None
	id = None
	first_name = ''
	last_name = ''

	def __init__(self, **kwargs):
		super(Owner, self).__init__(cls=self.__class__, **kwargs)

	def save(self):
		return super(Owner, self).save(self.__class__)

	def update(self):
		super(Owner, self).update(self.__class__)
		return self

	@staticmethod
	def list():
		return BaseModel.list(Owner)	

	def delete(id):
		BaseModel.delete(Owner, id)



class Animal:
	id = None
	birthday = None
	name = ''
	owner_id = ''


class Dog(Animal, BaseModel):

	def __init__(self, **kwargs):
		super(Dog, self).__init__(cls=self.__class__, **kwargs)


	def save(self):
		return super(Dog, self).save(self.__class__)

	def update(self):
		super(Dog, self).update(self.__class__)
		return self

	@staticmethod
	def list():
		return BaseModel.list(Dog)	

	def delete(id):
		BaseModel.delete(Dog, id)		



class Cat(Animal, BaseModel):

	def __init__(self, **kwargs):
		super(Cat, self).__init__(cls=self.__class__, **kwargs)

	def save(self):
		return super(Cat, self).save(self.__class__)

	def update(self):
		super(Cat, self).update(self.__class__)
		return self

	@staticmethod
	def list():
		return BaseModel.list(Cat)	

	def delete(id):
		BaseModel.delete(Cat, id)


